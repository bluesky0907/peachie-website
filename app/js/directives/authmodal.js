function AuthmodalDirective(Auth, $location, $firebaseObject) {
  'ngInject';
  return {
    restrict: 'E',
    templateUrl: 'directives/authmodal.html',
    controller: 'AuthModalCtrl'
  };
}

export default {
  name: 'authmodalDirective',
  fn: AuthmodalDirective
};