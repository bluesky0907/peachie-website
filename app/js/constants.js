const AppSettings = {
  appTitle: 'PeachyFit',
  apiUrl: '/api/v1'
};

export default AppSettings;
