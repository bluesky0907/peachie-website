function OnRun($rootScope, AppSettings, $location, Auth) {
  'ngInject';

  // change page title based on state
  $rootScope.$on('$stateChangeSuccess', (event, toState) => {
    $rootScope.pageTitle = '';
    
    if (toState.title) {
        $rootScope.pageTitle += toState.title;
        $rootScope.pageTitle += ' \u2014 ';
      }

      $rootScope.pageTitle += AppSettings.appTitle;
      angular.element('.modal').hide(); // same as first, but a bit angular way
  });

  $rootScope.$on('$routeChangeStart', function() {
      angular.element('.modal').hide(); // same as first, but a bit angular way
  })

  $rootScope.$on('$routeChangeError', function (event, next, previous, error) {
      // We can catch the error thrown when the $requireAuth promise is rejected
      // and redirect the user back to the home page
      console.log(error)
      if (error === 'AUTH_REQUIRED') {
          $location.path('/');
      }
  });

}

export default OnRun;
