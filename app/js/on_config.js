function OnConfig($stateProvider, $locationProvider, $urlRouterProvider, $compileProvider)
{
  'ngInject';
  if (process.env.NODE_ENV === 'production')
  {
    $compileProvider.debugInfoEnabled(false);
  }

  $locationProvider.html5Mode(
    {
      enabled:     true,
      requireBase: false
    }
  );
  //EXTREMELY Temporary Changes made: Coming Soon Applied.
  //TODO: Need to come back and update page titles and descriptions.
  $stateProvider
    .state(
      'Coming Soon', {
        url:         '/coming-soon',
        views:       {
          content: {
            controller:  'ComingSoonCtrl as comingsoon',
            templateUrl: 'coming-soon.html'
          }
        },
        title:       'Coming Soon',
        description: 'PeachyFit is on it\'s way! - Submit your email and we\'ll let you know when we\'re up and running!'
      }
    );
  $stateProvider
    .state(
      'Privacy', {
        url:         '/privacy',
        views:       {
          content: {
            controller:  'privacyCtrl as privacy',
            templateUrl: 'privacy.html'
          }
        },
        title:       'Privacy',
        description: 'PeachyFit is on it\'s way! - Submit your email and we\'ll let you know when we\'re up and running!'
      }
    );
  $stateProvider
    .state(
      'Home', {
        url:         '/',
        views:       {
          header:  {
            templateUrl: 'layout/transparentHeader.html'
          },
          content: {
            controller:  'HomepageCtrl as home',
            templateUrl: 'home.html'
          },
          footer:  {
            templateUrl: 'layout/footer.html'
          }
        },
        title:       'PeachyFit - The ultimate fitness challenge!',
        description: 'Peachy Fit is more than just a fitness program, it is a complete body sculpting challenge!'
      }
    );
  $stateProvider
    .state(
      'Testimonals', {
        url:         '/testimonials',
        views:       {
          header:  {
            // templateUrl: 'layout/header.html'
          },
          content: {
            controller:  'TestimonalsCtrl as testimonials',
            templateUrl: 'testimonials.html'
          },
          footer:  {
            // templateUrl: 'layout/footer.html'
          }
        },
        title:       'Testimonials - What our users say | PeachyFit',
        description: 'Find out what our community of PeachyFit girls have to say about us!'
      }
    );
  $stateProvider
    .state(
      'FAQs', {
        url:         '/faqs',
        views:       {
          header:  {
            // templateUrl: 'layout/header.html'
          },
          content: {
            controller:  'FaqsCtrl as faqs',
            templateUrl: 'faqs.html'
          },
          footer:  {
            // templateUrl: 'layout/footer.html'
          }
        },
        title:       'Frequently Asked Questions | PeachyFit',
        description: 'Got any questions about PeachyFit or after some advice, let us know!'
      }
    );
  $stateProvider
    .state(
      'Pricing', {
        url:         '/pricing',
        views:       {
          header:  {
            templateUrl: 'layout/header.html'
          },
          content: {
            controller:  'PricingCtrl as pricing',
            templateUrl: 'pricing.html'
          },
          footer:  {
            templateUrl: 'layout/footer.html'
          }
        },
        title:       'Our Pricing | PeachyFit',
        description: 'Maximum Fitness. Minimally Priced.'
      }
    );
  $stateProvider
    .state(
      'Contact', {
        url:         '/contact',
        views:       {
          header:  {
            templateUrl: 'layout/header.html'
          },
          content: {
            controller:  'ContactCtrl as contact',
            templateUrl: 'contact.html'
          },
          footer:  {
            templateUrl: 'layout/footer.html'
          }
        },
        title:       'Contact Us | PeachyFit',
        description: 'Get in touch with the PeachyFit staff'
      }
    );
  $stateProvider
    .state(
      'Secure', {
        url:   '/secure',
        views: {
          header:  {
            templateUrl: 'layout/secureheader.html'
          },
          content: {
            controller:  'SecureCtrl as secure',
            templateUrl: 'secure.html'
          },
          footer:  {
            templateUrl: 'layout/footer.html'
          }
        },
        title: 'Purchase Bundle | PeachyFit',
        description: 'Upgrade your fitness and purchase one of our bundles! Instantly start your body transformation challenge!',
        resolve: {
            "currentAuth": ["Auth", function (Auth) {
                    // $requireAuth returns a promise so the resolve waits for it to complete
                    // If the promise is rejected, it will throw a $stateChangeError (see above)
                    // console.log(Auth.$requireSignIn().$$state.status)
                    // $scope.authstatus = Auth.$requireSignIn();
                    return Auth.$requireSignIn();
                }]
        }
      }
    );
  $stateProvider
    .state(
      'SecureCompletion', {
        url:   '/secure/completed',
        views: {
          header:  {
            templateUrl: 'layout/secureheader.html'
          },
          content: {
            controller:  'SecureCompletionCtrl as secure',
            templateUrl: 'securecompletion.html'
          },
          footer:  {
            templateUrl: 'layout/footer.html'
          }
        },
        title: 'Purchase Completed! | PeachyFit',
        description: 'You have completed your PeachyFit purchase. Your bundle is on its way, your challenge starts today!'
      }
    );
  $stateProvider
    .state(
      'About', {
        url:   '/about',
        views: {
          header:  {
            templateUrl: 'layout/header.html'
          },
          content: {
            controller:  'AboutCtrl as about',
            templateUrl: 'about.html'
          },
          footer:  {
            templateUrl: 'layout/footer.html'
          }
        },
        title: 'About us - Our Mission | PeachyFit',
        description: 'Find out about PeachyFit, Our Vision and our Mission.'
      }
    );
  $stateProvider
    .state(
      'Challenge', {
        url:   '/fitness-challenge',
        views: {
          header:  {
            templateUrl: 'layout/header.html'
          },
          content: {
            controller:  'ChallengeCtrl as challenge',
            templateUrl: 'fitness-challenge.html'
          },
          footer:  {
            templateUrl: 'layout/footer.html'
          }
        },
        title: '6 Week Body Sculpting Challenge | PeachyFit',
        description: ''
      }
    );
  $stateProvider
    .state(
      'Bundle', {
        url:   '/bundle',
        views: {
          header:  {
            templateUrl: 'layout/header.html'
          },
          content: {
            controller:  'BundleCtrl as bundle',
            templateUrl: 'bundle.html'
          },
          footer:  {
            templateUrl: 'layout/footer.html'
          }
        },
        title: 'The Perfect PeachyFit Bundle - Get started today! | PeachyFit',
        description: ''
      }
    );
  $stateProvider
  .state(
    'Ambassadors', {
      url:   '/peachyfit-ambassadors',
      views: {
        header:  {
          templateUrl: 'layout/header.html'
        },
        content: {
          controller:  'AmbassadorCtrl as ambassador',
          templateUrl: 'ambassadors.html'
        },
        footer:  {
          templateUrl: 'layout/footer.html'
        }
      },
      title: 'The Perfect PeachyFit Bundle - Get started today! | PeachyFit',
      description: ''
    }
  );
  $urlRouterProvider.otherwise('/');

}

export default OnConfig;
