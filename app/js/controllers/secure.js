function SecureCtrl($scope, $http, $location) {
	'ngInject';

  var formatData = function(data){
      let returnData = '';
      let count = 0;
      for (let i in data){
          if(count == 0){
              returnData += i+'='+data[i];
          }else{
              returnData += '&'+i+'='+data[i];
          }
          count = count + 1;
      }
      return returnData;
  }

  var test_mode = true;
  var NOODLIO_PAY_API_URL = 'https://noodlio-pay.p.mashape.com';
  var NOODLIO_PAY_API_KEY = '3fEagjJCGAmshMqVnwTR70bVqG3yp1lerJNjsnTzx5ODeOa99V';
  var STRIPE_ACCOUNT_ID = 'acct_19N1NnHauOISLCG7';

  // $http.defaults.headers.common['X-Mashape-Key'] = NOODLIO_PAY_API_KEY;
  // $http.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded';
  // $http.defaults.headers.common['Accept'] = 'application/json';

  var headers = {
    'X-Mashape-Key': NOODLIO_PAY_API_KEY,
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept': 'application/json'
  };

  var mailgunUrl = "https://cors-anywhere.herokuapp.com/https://api.mailgun.net/v3/sandboxa109a4dd29934e06a468bacac8e5f703.mailgun.org/messages";
  var mailgunApiKey = window.btoa("api:key-973b69c3dc26fd40f718b2184ca956fc");

  $scope.FormData = {
  	number: '4242424242424242',
  	cvc: '256',
  	exp_month: '08',
  	exp_year: '2018',
    address: '',
    zip_code: '',
    country: '',
    phoneNumber: '',

  	test: test_mode,
  };

    $scope.createToken = function () {
      // init for the DOM
      $scope.ResponseData = {
        loading: true
      };

      $('#checkout-button').prop('disabled', true)
      // create a token and validate the credit card details
      $http({
        method: 'POST',
        url: NOODLIO_PAY_API_URL + '/tokens/create',
        data: formatData($scope.FormData), 
        headers: headers
      }).then(
           function (response) {
                // --> success
                response = response.data;
                console.log(response);
                if (response.hasOwnProperty('id')) {
                    var token = response.id;
                    $scope.ResponseData['token'] = token;
                     $scope.proceedCharge(token);
                } else {
                    $scope.ResponseData['token'] = 'Error, see console';
                    $scope.ResponseData['loading'] = false;
                };
            }
        ).catch(
            function (response) {
                console.log(response)
                $scope.ResponseData['token'] = 'Error, see console';
                $scope.ResponseData['loading'] = false;
            }
        );
    };

    // charge the customer with the token
    $scope.proceedCharge = function (token) {
        var param = {
            source: token,
            amount: 9900,
            currency: 'usd',
            description: 'Your custom description here',
            stripe_account: STRIPE_ACCOUNT_ID,
            test: test_mode,
        };

        $http({
          method: 'POST',
          url: NOODLIO_PAY_API_URL + '/charge/token',
          data: formatData(param), 
          headers: headers
        }).then(function (response) {
                // --> success
                console.log(response);

                $scope.firebaseUser = firebase.auth().currentUser;
                var ref = firebase.database().ref('users/' + firebase.auth().currentUser.uid);
                ref.update({
                  address: $scope.FormData.address,
                  zip_code: $scope.FormData.zip_code,
                  country: $scope.FormData.country,
                  isPaid: true,
                  phoneNumber: $scope.FormData.phoneNumber,
                  time_paid: new Date().getTime()
                });

                $scope.sendEmail(firebase.auth().currentUser.email);
                $location.path('/secure/completed');

                $scope.ResponseData['loading'] = false;
                if (response.hasOwnProperty('id')) {
                    var paymentId = response.id;
                    $scope.ResponseData['paymentId'] = paymentId;
                } else {
                    $scope.ResponseData['paymentId'] = 'Error, see console';
                };
	        }).catch(function (response) {
                console.log(response)
                $scope.ResponseData['paymentId'] = 'Error, see console';
                $scope.ResponseData['loading'] = false;
	        });
    };

    $scope.sendEmail = function(recipient) {
      var subject = "Test Peachyfit website";
      var message = "Thanks for purchase!";
      console.log(recipient);
        $http(
          {
            "method": "POST",
            "url": mailgunUrl,
            "headers": {
              "Content-Type": "application/x-www-form-urlencoded",
              "Authorization": "Basic " + mailgunApiKey
            },
          data: "from=" + "fitness.project2211@gmail.comm" + "&to=" + recipient + "&subject=" + subject + "&text=" + message
        }
        ).then(function(success) {
            console.log("SUCCESS " + JSON.stringify(success));
        }, function(error) {
            console.log("ERROR " + JSON.stringify(error));
        });
    }
}



export default {
  name: 'SecureCtrl',
  fn: SecureCtrl
};
