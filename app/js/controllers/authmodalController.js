function AuthModalCtrl($scope, Auth, $location, $firebaseObject) {
	'ngInject';
  	$scope.auth = Auth;
    // any time auth state changes, add the user data to scope
    $scope.auth.$onAuthStateChanged(function (firebaseUser) {
        $scope.firebaseUser = firebaseUser;
    });
    $scope.signIn = function () {
        $scope.auth.$signInWithEmailAndPassword($scope.email, $scope.pwd).then(function (firebaseUser) {
            $scope.firebaseUser = firebaseUser;
            $('.help-block.login .ok').removeClass('hidden');
            $('.help-block.login .spinner').addClass('hidden');
            // $('#authModal').modal('toggle');
            $location.path('/bundle');
        }).catch(function (error) {
            $scope.error = error;
            console.log(error);
            $('.help-block.login span').text($scope.error.message);
            $('.help-block.login .spinner').addClass('hidden');
        });
    };
    $scope.signUp = function () {
        console.log($scope.email, $scope.pwd)
        $scope.auth.$createUserWithEmailAndPassword($scope.email, $scope.pwd).then(function (firebaseUser) {
            $scope.firebaseUser = firebaseUser;
            var ref = firebase.database().ref();
            $scope.profile = $firebaseObject(ref.child('users').child(firebaseUser.uid));
            $scope.profile.createdAt = Date.now();
            $scope.profile.email = firebaseUser.email;
            $scope.profile.id = firebaseUser.uid;
            $scope.profile.isPaid = false;
            $scope.profile.name = $scope.name;
            $scope.profile.$save();
            $('.help-block.register .ok').removeClass('hidden');
            $('.help-block.register .spinner').addClass('hidden');
            // $('#authModal').modal('toggle');
            $location.path('/secure');
        }).catch(function (error) {
            $scope.error = error;
            $('.help-block.register span').text($scope.error.message);
            $('.help-block.register .spinner').addClass('hidden');
        });
    };

    $scope.signOut = function () {
        $scope.auth.$signOut().then(function () {
            console.log('Sign-out successful');
            $location.path('/homepage');
            // Sign-out successful.
        }, function (error) {
            // An error happened.
            console.log('An error happened');
            console.log(error)
        });
    };

}

export default {
  name: 'AuthModalCtrl',
  fn: AuthModalCtrl
};
