import config from '../config';
import gulp   from 'gulp';

gulp.task('watch', ['browserSync'], function() {

  global.isWatching = true;

  // Scripts are automatically watched and rebundled by Watchify inside Browserify task
  gulp.watch(config.scripts.src, ['eslint']);
  gulp.watch(config.styles.src,  ['styles']);
  gulp.watch(config.images.src,  ['images']);
  gulp.watch(config.fonts.src,   ['fonts']);
  gulp.watch(config.views.watch, ['views']);
  gulp.watch(config.scripts.src, function (e) {
    if (e.type == 'added') {
      var baseDir = path.resolve('./app'),
        relativePath = e.path.replace(baseDir + '/', ''),
        moduleDir = relativePath.split('/')[0],
        moduleFile = baseDir + '/' + moduleDir + '/module.js';

      fs.utimes(moduleFile, new Date(), new Date(), function () {
        console.log('Updated module file mtime to trigger watchers', moduleFile);
      });
    }

  });

});